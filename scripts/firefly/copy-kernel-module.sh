#!/bin/bash
TOP_DIR=$1
ROOTFS_DIR=$2

KERNEL_DIR="$TOP_DIR/../../kernel"

if [ -d "$KERNEL_DIR" ];then
    cd $KERNEL_DIR
    VERSION=$(cat Makefile | sed -n "s/^VERSION\ = \([0-9]*\)/\1/p")
    PATCHLEVEL=$(cat Makefile | sed -n "s/^PATCHLEVEL\ = \([0-9]*\)/\1/p")
    SUBLEVEL=$(cat Makefile | sed -n "s/^SUBLEVEL\ = \([0-9]*\)/\1/p")

    LINUX_VERSION="$VERSION.$PATCHLEVEL.$SUBLEVEL"
    mkdir -p $ROOTFS_DIR/lib/modules/$LINUX_VERSION
    
    for i in $(find . -name '*.ko')
    do
        cp $i $ROOTFS_DIR/lib/modules/$LINUX_VERSION
    done
    cd -
fi
